package com.nogroup.ecommerce.business.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nogroup.ecommerce.data.entities.User;
import com.nogroup.ecommerce.data.repositories.UserRepository;
import com.nogroup.ecommerce.data.utils.PasswordUtils;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;

	public User checkCredentials(String email, String password) {
		for (User tmp : userRepository.findAll()) {
			String mySecurePassword = PasswordUtils.generateSecurePassword(password, tmp.getSalt());

			if(tmp.getEmail().equals(email) &&  mySecurePassword.equals(tmp.getPassword())) {
				return tmp;
			}
		}
		
		return null;
	}

	public String createNewUser(String firstName, String lastName, String email, String password, String phone,
			String address) {
		
		User user = new User();
		user.setfName(firstName);
		user.setlName(firstName);
		user.setAddress(address);
		user.setPic("");
		user.setPhone(phone);
		user.setEmail(email);
		
		
		String uuid = password ;
        // Generate Salt. The generated value can be stored in DB. 
        String salt = PasswordUtils.getSalt(30);
        user.setSalt(salt);
        // Protect user's password. The generated value can be stored in DB.
        String mySecurePassword = PasswordUtils.generateSecurePassword(uuid, salt);
		user.setPassword(mySecurePassword);
		
		for (User tmp : userRepository.findAll()) {
			if(tmp.getEmail().equals(email)) {
				return "100";
			}
		}
		
		try {
			userRepository.save(user);
			return "200";
		}catch (Exception e) {
			return "400";
		}
	}
}
