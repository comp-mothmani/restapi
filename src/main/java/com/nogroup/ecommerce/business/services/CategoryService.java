package com.nogroup.ecommerce.business.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nogroup.ecommerce.data.entities.Category;
import com.nogroup.ecommerce.data.models.CategoryTemp;
import com.nogroup.ecommerce.data.repositories.CategoryRepository;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;

	
	public List<Category> getAllCategories() {
		
		  return categoryRepository.findAll();
	}


	public List<CategoryTemp> getCategories() {
		List<CategoryTemp> categories = new ArrayList<CategoryTemp>();
		for (Category tmp : categoryRepository.findAll()) {
			categories.add(new CategoryTemp(tmp.getId(),tmp.getName(), tmp.getPic(), tmp.getDescription()));
		}
		return categories;
	}


	public Category getSCByCName(String name) {
		for (Category tmp : categoryRepository.findAll()) {
			if(tmp.getName().equals(name)) {
				return tmp;
			}
		}
		return null;
	}


	
}
