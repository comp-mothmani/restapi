package com.nogroup.ecommerce.business.services;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.nogroup.ecommerce.data.entities.Purchase;
import com.nogroup.ecommerce.data.entities.SubCategory;
import com.nogroup.ecommerce.data.models.PurchaseTemp;
import com.nogroup.ecommerce.data.repositories.PurchaseRepository;
import com.nogroup.ecommerce.data.repositories.SubCategoryRepository;
import com.nogroup.ecommerce.data.repositories.UserRepository;

@Service
public class PurchaseService {

	@Autowired
	PurchaseRepository purchaseRepository;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	SubCategoryRepository subCategoryRepository;

	public String saveNewPurchase(PurchaseTemp temp, int userId) {
		
		Purchase purchase = new Purchase();
		purchase.setDate(new Date());
		purchase.setUser(userRepository.findById(userId).get());
		
		
		for (SubCategory tmp : subCategoryRepository.findAll()) {
			if(tmp.getId() == temp.getProductId()) {
				temp.setProductPic(tmp.getPic());
				System.out.println("yes found");
				break;
			}
		}

		ObjectMapper objectMapper = new ObjectMapper();
		String s = "";
		try {
			s = objectMapper.writeValueAsString(temp);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		purchase.setContent(s);

		try {
			purchaseRepository.save(purchase);
			return "200";
		} catch (Exception e) {
			return "400";
		}

	}

	public String fetchPurchasesByUserId(int userId) throws JsonProcessingException {
		List<PurchaseTemp> tmps = new ArrayList<PurchaseTemp>();
		for (Purchase tmp : purchaseRepository.findAll()) {
			if (tmp.getUser().getId() == userId) {
				tmps.add(new ObjectMapper().readValue(tmp.getContent(), PurchaseTemp.class));
			}
		}
		return new ObjectMapper().writeValueAsString(tmps);
	}
}
