package com.nogroup.ecommerce.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nogroup.ecommerce.data.entities.Category;


@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> /*<ENTITY, TYPE OF ID PARMETER >*/  {

}
