package com.nogroup.ecommerce.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nogroup.ecommerce.data.entities.SubCategory;


@Repository
public interface SubCategoryRepository extends JpaRepository<SubCategory, Integer> /*<ENTITY, TYPE OF ID PARMETER >*/  {

}
