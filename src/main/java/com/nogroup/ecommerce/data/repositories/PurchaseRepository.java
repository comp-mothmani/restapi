package com.nogroup.ecommerce.data.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nogroup.ecommerce.data.entities.Purchase;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, Integer> /*<ENTITY, TYPE OF ID PARMETER >*/  {

}
