package com.nogroup.ecommerce.data.models;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "productName", "productPrice", "productPic", "productQuantity" })
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseTemp implements Serializable {

	/**
	 * @author mahdi othmani
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("productName")
	private String productName;
	@JsonProperty("productPrice")
	private String productPrice;
	@JsonProperty("productPic")
	private String productPic;
	@JsonProperty("productQuantity")
	private Integer productQuantity;
	@JsonProperty("productId")
	private Integer productId;
	
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("productName")
	public String getProductName() {
		return productName;
	}

	@JsonProperty("productName")
	public void setProductName(String productName) {
		this.productName = productName;
	}

	@JsonProperty("productPrice")
	public String getProductPrice() {
		return productPrice;
	}

	@JsonProperty("productPrice")
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}

	@JsonProperty("productPic")
	public String getProductPic() {
		return productPic;
	}

	@JsonProperty("productPic")
	public void setProductPic(String productPic) {
		this.productPic = productPic;
	}

	@JsonProperty("productQuantity")
	public Integer getProductQuantity() {
		return productQuantity;
	}

	@JsonProperty("productQuantity")
	public void setProductQuantity(Integer productQuantity) {
		this.productQuantity = productQuantity;
	}

	@JsonProperty("productId")
	public Integer getProductId() {
		return productId;
	}
	
	@JsonProperty("productId")
	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@Override
	public String toString() {
		return "PurchaseTemp [productName=" + productName + ", productPrice=" + productPrice + ", productPic="
				+ productPic + ", productQuantity=" + productQuantity + ", productId=" + productId + "]";
	}

	

	

	
}
