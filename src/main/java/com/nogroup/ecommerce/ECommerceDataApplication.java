package com.nogroup.ecommerce;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.nogroup.ecommerce.data.repositories.CategoryRepository;

@SpringBootApplication
public class ECommerceDataApplication  implements CommandLineRunner{

    private static final Logger log = LoggerFactory.getLogger(ECommerceDataApplication.class);

	@Autowired
    private CategoryRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(ECommerceDataApplication.class, args);
	}
	
	@Override
    public void run(String... args) {

        log.info("StartApplication...");

  

        /*System.out.println("\nfindAll()");
        try {
        	repository.findAll().forEach(x -> System.out.println(x));
        }catch (Exception e) {
        	System.out.println("ERROR: "+e);
        }*/

       

    }

}
