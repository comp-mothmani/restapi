package com.nogroup.ecommerce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.nogroup.ecommerce.business.services.PurchaseService;
import com.nogroup.ecommerce.data.models.PurchaseTemp;

@RestController
public class PurchaseController {

	@Autowired
	PurchaseService purchaseService;

	@PostMapping("/puchases/save")
	public String savePurchase(@RequestParam String purchase, @RequestParam int userId) {
		/***
		 * IMPORTANT: String purchase ==> the variable name must be identical with the
		 * key sent in the client params
		 */

		List<PurchaseTemp> tmps = null;
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			CollectionType typeReference = TypeFactory.defaultInstance().constructCollectionType(List.class,
					PurchaseTemp.class);
			tmps = objectMapper.readValue(purchase, typeReference);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			for (PurchaseTemp tmp : tmps) {
				purchaseService.saveNewPurchase(tmp, userId);
			}
			return "200";

		} catch (Exception e) {
			return "400";
		}

	}
	
	@GetMapping("/purchases/fetch/{userId}")
	public String fetchPurchasesByUserId(@PathVariable int userId) throws JsonProcessingException {
		return purchaseService.fetchPurchasesByUserId(userId);
	}
}
