package com.nogroup.ecommerce.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.nogroup.ecommerce.business.services.CategoryService;
import com.nogroup.ecommerce.data.entities.Category;
import com.nogroup.ecommerce.data.entities.SubCategory;
import com.nogroup.ecommerce.data.models.CategoryTemp;

@RestController
public class CategoryController {
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/api")
	public List<Category> getAll() {
		return categoryService.getAllCategories();
	}

	@GetMapping("/categories")
	public List<CategoryTemp> getCategories() {
		return categoryService.getCategories();
	}
	
	@GetMapping("/categories/{name}")
	public List<SubCategory> getSCByCName(@PathVariable String name) {
		return categoryService.getSCByCName(name).getSubCategories();
	}
	
	
}
