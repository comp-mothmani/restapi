package com.nogroup.ecommerce.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.nogroup.ecommerce.business.services.UserService;
import com.nogroup.ecommerce.data.entities.User;

@RestController
public class UsersController {

	@Autowired
	UserService userService;

	@GetMapping("/users/check/{email}/{password}")
	public User checkCredentials(@PathVariable String email, @PathVariable String password) {
		return userService.checkCredentials(email, password);
	}

	@GetMapping("/users/create/{firstName}/{lastName}/{email}/{password}/{phone}/{address}")
	public String createNewUser(@PathVariable String firstName, 
								@PathVariable String lastName, 
								@PathVariable String email,
			                    @PathVariable String password, 
			                    @PathVariable String phone,
			                    @PathVariable String address) {
			 return userService.createNewUser(firstName,lastName,email, password,phone,address);
	}

}
